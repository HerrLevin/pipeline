# Biletado Pipelines
This repository contains gitlab-pipeline templates to include in other biletado projects.

## usage
In your projects `.gitlab-ci.yaml`:
```yaml
include:
  - project: 'biletado/pipeline'
    ref: "1"
    file: '/job-name.yml'
```

Includes should always use a tag as reference as this repository is not planned to have release-branches.

## jobs
### docker-build
Based on the official docker-build template it adds tagging with the name of git-tags as well as docker-tags with the commit_short_sha.

Don't forget to [cleanup images tags](https://gitlab.com/help/user/packages/container_registry/reduce_container_registry_storage#cleanup-policy),
e.g. with the pattern `[0-9a-f]{8}`

> TODO: option to disable tagging with branch-names or tagging only if branch is protected
